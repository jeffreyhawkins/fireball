package com.hawkins.fireball.services;

import com.hawkins.fireball.models.Company;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by jhawkins on 7/21/16.
 */
@Ignore
public class CompanyServiceTest {


    @Test
    public void testGetAllCompanies() {
        CompanyService companyService = new CompanyService();
        List<Company> companies = companyService.get();
        assertEquals(3, companies.size());
        assertEquals("CompanyService", companyService.toString());
    }
}
