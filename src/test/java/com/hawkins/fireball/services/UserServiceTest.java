package com.hawkins.fireball.services;

import com.hawkins.fireball.models.User;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

/**
 * Created by jhawkins on 7/21/16.
 */
@Ignore
public class UserServiceTest {


    @Test
    public void testGetAllUsers() {
        UserService usersService = new UserService();
        List<User> users = usersService.get(anyString());
        assertEquals(5, users.size());
        assertEquals("UserService", usersService.toString());
    }
}
