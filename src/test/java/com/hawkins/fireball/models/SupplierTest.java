package com.hawkins.fireball.models;

import com.hawkins.fireball.builders.SupplierBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by jhawkins on 7/21/16.
 */
public class SupplierTest {

    @Test
    public void testSupplier() {
        Supplier supplier = new SupplierBuilder()
                .setId("id")
                .setCompanyId("companyId")
                .setName("name")
                .setContact("contact")
                .setAddress("address")
                .setCity("city")
                .setState("state")
                .setZip("zip")
                .setPhone("phone")
                .setWebsite("website")
                .build();

        assertEquals("id", supplier.getId());
        assertEquals("companyId", supplier.getCompanyId());
        assertEquals("name", supplier.getName());
        assertEquals("contact", supplier.getContact());
        assertEquals("address", supplier.getAddress());
        assertEquals("city", supplier.getCity());
        assertEquals("state", supplier.getState());
        assertEquals("zip", supplier.getZip());
        assertEquals("phone", supplier.getPhone());
        assertEquals("website", supplier.getWebsite());

        assertNotNull(supplier.toString());
    }
}
