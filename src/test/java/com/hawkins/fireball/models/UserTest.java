package com.hawkins.fireball.models;

import com.hawkins.fireball.builders.UserBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by jhawkins on 7/21/16.
 */
public class UserTest {

    @Test
    public void testUser() {
        User user = new UserBuilder()
                .setId("id")
                .setCompanyId("companyId")
                .setName("name")
                .setEmail("email")
                .setPassword("password")
                .build();

        assertEquals("id", user.getId());
        assertEquals("companyId", user.getCompanyId());
        assertEquals("name", user.getName());
        assertEquals("email", user.getEmail());
        assertEquals("password", user.getPassword());

        assertNotNull(user.toString());
    }
}
