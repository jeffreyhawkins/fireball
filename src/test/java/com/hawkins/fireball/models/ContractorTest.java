package com.hawkins.fireball.models;

import com.hawkins.fireball.builders.ContractorBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by jhawkins on 7/21/16.
 */
public class ContractorTest {

    @Test
    public void testContractor() {
        Contractor contractor = new ContractorBuilder()
                .setId("id")
                .setCompanyId("companyId")
                .setName("name")
                .setContact("contact")
                .setCategory("category")
                .setAddress("address")
                .setCity("city")
                .setState("state")
                .setZip("zip")
                .setPhone("phone")
                .setWebsite("website")
                .build();

        assertEquals("id", contractor.getId());
        assertEquals("companyId", contractor.getCompanyId());
        assertEquals("name", contractor.getName());
        assertEquals("contact", contractor.getContact());
        assertEquals("category", contractor.getCategory());
        assertEquals("address", contractor.getAddress());
        assertEquals("city", contractor.getCity());
        assertEquals("state", contractor.getState());
        assertEquals("zip", contractor.getZip());
        assertEquals("phone", contractor.getPhone());
        assertEquals("website", contractor.getWebsite());

        assertNotNull(contractor.toString());
    }
}
