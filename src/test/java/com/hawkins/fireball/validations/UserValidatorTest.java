package com.hawkins.fireball.validations;

import com.hawkins.fireball.models.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by jhawkins on 8/24/16.
 */
public class UserValidatorTest {

    @Test
    public void testNullUser() {
        assertEquals(false, UserValidator.isValid(null));
    }


    @Test
    public void testValidUser() {
        User user = new User("100", "100", "first", "last", "sdfsf");
        assertEquals(true, UserValidator.isValid(user));
    }


    @Test
    public void testNullFirstName() {
        User user = new User("100", "100", null, "last", "sdfsd");
        assertEquals(false, UserValidator.isValid(user));
    }


    @Test
    public void testNullLastName() {
        User user = new User("100", "100", "first", null, "dfadf");
        assertEquals(false, UserValidator.isValid(user));
    }


    @Test
    public void testEmptyFirstName() {
        User user = new User("100", "100", "", "last", "dfsdf");
        assertEquals(false, UserValidator.isValid(user));
    }


    @Test
    public void testEmptyLastName() {
        User user = new User("100", "100", "first", null, "dfadsa");
        assertEquals(false, UserValidator.isValid(user));
    }


    @Test
    public void testFirstNameTooLong() {
        User user = new User("100", "100", "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789", "last", "dsfad");
        assertEquals(false, UserValidator.isValid(user));
    }


    @Test
    public void testLastNameTooLong() {
        User user = new User("100", "100", "first", "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789", "dsfd");
        assertEquals(false, UserValidator.isValid(user));
    }
}
