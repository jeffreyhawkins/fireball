package com.hawkins.fireball.validations;

import com.hawkins.fireball.models.Company;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by jhawkins on 8/24/16.
 */
public class CompanyValidatorTest {

    @Test
    public void testNullCompany() {
        assertEquals(false, CompanyValidator.isValid(null));
    }


    @Test
    public void testValidCompany() {
        Company company = new Company("100", "Demo Inc.");
        assertEquals(true, CompanyValidator.isValid(company));
    }


    @Test
    public void testNullName() {
        Company company = new Company("100", null);
        assertEquals(false, CompanyValidator.isValid(company));
    }


    @Test
    public void testEmptyName() {
        Company company = new Company("100", "");
        assertEquals(false, CompanyValidator.isValid(company));
    }


    @Test
    public void testNameTooLong() {
        Company company = new Company("100", "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
        assertEquals(false, CompanyValidator.isValid(company));
    }
}
