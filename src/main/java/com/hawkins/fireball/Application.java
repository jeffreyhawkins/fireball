package com.hawkins.fireball;

import com.google.gson.Gson;
import com.hawkins.fireball.controllers.CompanyController;
import com.hawkins.fireball.controllers.ContractorController;
import com.hawkins.fireball.controllers.SupplierController;
import com.hawkins.fireball.controllers.UserController;
import com.hawkins.fireball.services.CompanyService;
import com.hawkins.fireball.services.ContractorService;
import com.hawkins.fireball.services.SupplierService;
import com.hawkins.fireball.services.UserService;
import com.hawkins.fireball.utils.Constants;
import com.hawkins.fireball.utils.ResponseError;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;

import static spark.Spark.before;
import static spark.Spark.exception;
import static spark.Spark.halt;
import static spark.Spark.port;
import static spark.Spark.staticFileLocation;

/**
 * Created by jhawkins on 7/21/16.
 */
public class Application {
    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        Application application = new Application();
        application.go();
    }


    public void go() {
        LOG.info("Application::go");

        // We will run on port 8080 if no other port is specified...
        port(Integer.parseInt(System.getProperty("PORT", "8080")));

        staticFileLocation("/public");

        // Log all requests.
        before((req, res) -> {
            LOG.info("Handling request - " + req.requestMethod() + " - " + req.uri());
        });

        // Check for required headers.
        before((req, res) -> {
//            if (StringUtils.isBlank(req.headers(Constants.ORGOID))) {
//                LOG.info("Missing required header.");
//                halt(HttpServletResponse.SC_BAD_REQUEST, "Missing required header.");
//            }
        });


        // Handle any IllegalArgumentException's.
        exception(IllegalArgumentException.class, (error, req, res) -> {
            LOG.error("IllegalArgumentException", error);
            res.status(HttpServletResponse.SC_BAD_REQUEST);
            res.body(toJson(new ResponseError(error)));
        });


        // Handle any IllegalStateException's.
        exception(IllegalStateException.class, (error, req, res) -> {
            LOG.error("IllegalStateException", error);
            res.status(HttpServletResponse.SC_BAD_REQUEST);
            res.body(toJson(new ResponseError(error)));
        });


        // Let's get down to business
        new CompanyController(new CompanyService());
        new UserController(new UserService());
        new SupplierController(new SupplierService());
        new ContractorController(new ContractorService());
    }

    private String toJson(Object object) {
        return new Gson().toJson(object);
    }

}
