package com.hawkins.fireball.mocks;

import com.hawkins.fireball.models.Company;
import com.hawkins.fireball.builders.CompanyBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jhawkins on 7/21/16.
 */
public class MockCompanies {
    private static final Logger LOG = LoggerFactory.getLogger(MockCompanies.class);

    public MockCompanies() {}

    public static List<Company> getCompanies() {
        LOG.info("MockCompanies::getCompanies");

        List<Company> companies = new ArrayList<>();

        companies.add(create("ADP l.l.c."));
        companies.add(create("HawkinsLabs l.l.c."));
        companies.add(create("Ford"));

        return companies;
    }


    private static Company create(String name) {
        LOG.info("MockCompanies::create");

        Company company = new CompanyBuilder()
                .setId(UUID.randomUUID().toString())
                .setName(name)
                .build();

        return company;
    }
}
