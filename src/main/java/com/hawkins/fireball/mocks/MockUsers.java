package com.hawkins.fireball.mocks;

import com.hawkins.fireball.models.User;
import com.hawkins.fireball.builders.UserBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jhawkins on 7/21/16.
 */
public class MockUsers {
    private static final Logger LOG = LoggerFactory.getLogger(MockUsers.class);

    public MockUsers() {}

    public static List<User> getUsers() {
        LOG.info("MockUsers::getUsers");

        List<User> users = new ArrayList<>();

        users.add(create("Herman", "Munster"));
        users.add(create("Lilly", "Munster"));
        users.add(create("Eddie", "Munster"));
        users.add(create("Marlyn", "Munster"));
        users.add(create("Grampa", "Munster"));

        return users;
    }


    private static User create(String first, String last) {
        LOG.info("MockUsers::create");

        User user = new UserBuilder()
                .setId(UUID.randomUUID().toString())
                //.setFirstName(first)
                //.setLastName(last)
                .build();

        return user;
    }
}
