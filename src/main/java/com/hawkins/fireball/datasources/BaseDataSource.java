package com.hawkins.fireball.datasources;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.sql2o.Sql2o;

import javax.sql.DataSource;
import java.util.UUID;

/**
 * Created by jhawkins on 7/22/16.
 */
public class BaseDataSource {
    private DataSource dataSource = null;
    private MongoDatabase mongoDatabase;
    private Sql2o sql2o;

    protected BaseDataSource() {}

    protected String getUUID() {
        String id = UUID.randomUUID().toString();
        id = id.replace("-", "");

        return id;
    }

    protected Sql2o getSql2o() {
        if (dataSource == null) {
            dataSource = DataSourceFactory.getDefaultFactory().getDataSource();
        }

        if (sql2o == null) {
            sql2o = new Sql2o(dataSource);
        }

        return sql2o;
    }

    protected MongoDatabase getMongoDatabase() {
        if (mongoDatabase == null) {
            mongoDatabase = DataSourceFactory.getDefaultFactory().getMongoDatabase();
        }

        return mongoDatabase;
    }

}
