package com.hawkins.fireball.datasources;

import com.hawkins.fireball.models.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;

import java.util.List;

/**
 * Created by jhawkins on 7/23/16.
 */
public class SupplierDataSource extends BaseDataSource {
    private static final Logger LOG = LoggerFactory.getLogger(SupplierDataSource.class);

    public void put(Supplier supplier) {
        LOG.info("SupplierDataSource::put");

        String sql = "insert into suppliers values(:id, :companyId, :contact, :name, :address," +
                " :city, :state, :zip, :phone, :website)";

        try (Connection connection = getSql2o().open()) {
            connection.createQuery(sql)
                    .addParameter(Supplier.ID, getUUID())
                    .addParameter(Supplier.COMPANY_ID, supplier.getCompanyId())
                    .addParameter(Supplier.CONTACT, supplier.getContact())
                    .addParameter(Supplier.NAME, supplier.getName())
                    .addParameter(Supplier.ADDRESS, supplier.getAddress())
                    .addParameter(Supplier.CITY, supplier.getCity())
                    .addParameter(Supplier.STATE, supplier.getState())
                    .addParameter(Supplier.ZIP, supplier.getZip())
                    .addParameter(Supplier.PHONE, supplier.getPhone())
                    .addParameter(Supplier.WEBSITE, supplier.getWebsite())
                    .executeUpdate();
        }
    }


    public List<Supplier> query() {
        LOG.info("SupplierDataSource::query");

        String sql = "select id, companyId, contact, name, address, city, state, zip, phone, website from suppliers";

        try (Connection connection = getSql2o().open()) {
            return connection.createQuery(sql).executeAndFetch(Supplier.class);
        }
    }
}
