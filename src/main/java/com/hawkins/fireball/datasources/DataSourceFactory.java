package com.hawkins.fireball.datasources;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by jhawkins on 7/24/16.
 */
public class DataSourceFactory {
    private static final Logger LOG = LoggerFactory.getLogger(DataSourceFactory.class);
    private static final String APPLICATION_PROPERTIES = "application.properties";
    private static final String DATABASE_URL = "database.url";
    private static final String DATABASE_USER = "database.user";
    private static final String DATABASE_PASSWORD = "database.password";
    private static final String DATABASE_INITIAL_SIZE = "database.pool.initial.size";
    private static final String MONGO_CONNECTION = "mongo.connection";
    private static final String MONGO_DATABASE = "mongo.database";
    private Properties properties = new Properties();
    private static DataSourceFactory instance = null;
    private DataSource dataSource;
    private MongoDatabase mongoDatabase;

    private DataSourceFactory() {

    }

    public synchronized static DataSourceFactory getDefaultFactory() {
        if (instance == null) {
            instance = new DataSourceFactory();
        }

        return instance;
    }


    public DataSource getDataSource() {
        if (dataSource == null) {
            dataSource = createDataSource();
        }

        return dataSource;
    }

    public MongoDatabase getMongoDatabase() {
        if (mongoDatabase == null) {
            mongoDatabase = createMongoDatabase();
        }

        return mongoDatabase;
    }


    private DataSource createDataSource() {
        LOG.info("DataSourceFactory::createDataSource");

        try (final InputStream stream =
                     BaseDataSource.class.getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES)) {
            properties.load(stream);
        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
        }

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setUrl(properties.getProperty(DATABASE_URL));
        basicDataSource.setUsername(properties.getProperty(DATABASE_USER));
        basicDataSource.setPassword(properties.getProperty(DATABASE_PASSWORD));
        basicDataSource.setInitialSize(Integer.parseInt(properties.getProperty(DATABASE_INITIAL_SIZE)));

        return basicDataSource;
    }

    private MongoDatabase createMongoDatabase() {
        LOG.info("DataSourceFactory::createMongoClient");

        try (final InputStream stream =
                     BaseDataSource.class.getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES)) {
            properties.load(stream);
        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
        }

        MongoClientURI uri = new MongoClientURI(properties.getProperty(MONGO_CONNECTION));
        MongoClient mongoClient = new MongoClient(uri);

        return mongoClient.getDatabase(properties.getProperty(MONGO_DATABASE));
    }
}
