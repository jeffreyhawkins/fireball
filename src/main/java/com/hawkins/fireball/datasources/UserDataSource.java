package com.hawkins.fireball.datasources;

import com.hawkins.fireball.models.User;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;

import java.util.List;

/**
 * Created by jhawkins on 7/23/16.
 */
public class UserDataSource extends BaseDataSource {
    private static final Logger LOG = LoggerFactory.getLogger(UserDataSource.class);


    public void put(User user) {
        LOG.info("UserDataSource::put");

        if (StringUtils.isEmpty(user.getId())) {
            add(user);
        } else {
            update(user);
        }
    }

    public List<User> query() {
        LOG.info("UserDataSource::query");

        String sql = "select id, first, last from users";

        try (Connection connection = getSql2o().open()) {
            return connection.createQuery(sql).executeAndFetch(User.class);
        }
    }

    private void add(User user) {
        LOG.info("UserDataSource::add");

//        String sql = "insert into users values(:id, :companyId, :first, :last)";
//
//        try (Connection connection = getSql2o().open()) {
//            connection.createQuery(sql)
//                    .addParameter(User.ID, getUUID())
//                    .addParameter(User.COMPANY_ID, user.getCompanyId())
//                    .addParameter(User.FIRST, user.getFirst())
//                    .addParameter(User.LAST, user.getLast())
//                    .executeUpdate();
//        }
    }

    private void update(User user) {
        LOG.info("UserDataSource::update");

//        String sql = "update users set first = :first, last = :last where id = :id";
//
//        try (Connection connection = getSql2o().open()) {
//            connection.createQuery(sql)
//                    .addParameter(User.ID, user.getId())
//                    .addParameter(User.FIRST, user.getFirst())
//                    .addParameter(User.LAST, user.getLast())
//                    .executeUpdate();
//        }
    }
}
