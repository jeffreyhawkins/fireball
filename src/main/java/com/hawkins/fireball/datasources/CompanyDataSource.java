package com.hawkins.fireball.datasources;

import com.hawkins.fireball.models.Company;
import com.mongodb.client.FindIterable;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jhawkins on 7/23/16.
 */
public class CompanyDataSource extends BaseDataSource {
    private static final Logger LOG = LoggerFactory.getLogger(CompanyDataSource.class);
    private static final String COLLECTION_NAME = "company";

    public void putMongo(Company company) {
        Document document = new Document().append(Company.NAME, company.getName());
        getMongoDatabase().getCollection(COLLECTION_NAME).insertOne(document);
    }

    public void put(Company company) {
        if (StringUtils.isEmpty(company.getId())) {
            add(company);
        } else {
            update(company);
        }
    }


    public List<Company> query(Company company) {
        LOG.info("CompanyDataSource::query");

        String sql = "select id, name from companies where name like :name";

        try (Connection connection = getSql2o().open()) {
            return connection.createQuery(sql)
                    .addParameter(Company.NAME, "%" + company.getName() + "%")
                    .executeAndFetch(Company.class);
        }
    }

    public List<Company> queryMongo() {
        LOG.info("CompanyDataSource::queryMongo");

        FindIterable<Document> items = getMongoDatabase().getCollection(COLLECTION_NAME).find();

        List<Company> list = new ArrayList<>();

        for (Document d : items) {
            list.add(new Company(d.getObjectId("_id").toString(), d.getString(Company.NAME)));
        }

        return list;
    }

    public List<Company> query() {
        LOG.info("CompanyDataSource::query");

        String sql = "select id, name from companies";

        try (Connection connection = getSql2o().open()) {
            return connection.createQuery(sql).executeAndFetch(Company.class);
        }
    }


    private void add(Company company) {
        LOG.info("CompanyDataSource::put");

        String sql = "insert into companies values(:id, :name)";

        try (Connection connection = getSql2o().open()) {
            connection.createQuery(sql)
                    .addParameter(Company.ID, getUUID())
                    .addParameter(Company.NAME, company.getName())
                    .executeUpdate();
        }
    }


    private void update(Company company) {
        LOG.info("CompanyDataSource::update");

        String sql = "update companies set name = :name where id = :id";

        try (Connection connection = getSql2o().open()) {
            connection.createQuery(sql)
                    .addParameter(Company.ID, company.getId())
                    .addParameter(Company.NAME, company.getName())
                    .executeUpdate();
        }
    }
}
