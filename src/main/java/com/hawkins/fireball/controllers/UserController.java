package com.hawkins.fireball.controllers;

import com.hawkins.fireball.services.UserService;
import com.hawkins.fireball.utils.Constants;
import com.hawkins.fireball.utils.JsonTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.post;
import static spark.Spark.put;

/**
 * Created by jhawkins on 7/21/16.
 */
public class UserController extends BaseController {
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
    private UserService userService;

    public UserController(UserService userService) {
        LOG.info("UserController::ctor");

        this.userService = userService;

        get("/rest/v1/users", (req, res) -> {
            String company = req.headers(Constants.ORGOID);

            res.type(Constants.APPLICATION_JSON);
            return userService.get(company);
        }, new JsonTransformer());


        post("/rest/v1/users", (req, res) -> {
            String body = req.body();
            if (StringUtils.isBlank(body) == true) {
                halt(HttpServletResponse.SC_BAD_REQUEST, "Missing body.");
            }

            Map<String, String> map = getGson().fromJson(body, Map.class);

            //userService.add(map.get(User.COMPANY_ID), map.get(User.FIRST), map.get(User.LAST));

            return StringUtils.EMPTY;
        });


        put("/rest/v1/users", (req, res) -> {
            String body = req.body();
            if (StringUtils.isBlank(body) == true) {
                halt(HttpServletResponse.SC_BAD_REQUEST, "Missing body.");
            }

            Map<String, String> map = getGson().fromJson(body, Map.class);

            //userService.update(map.get(User.ID), map.get(User.COMPANY_ID), map.get(User.FIRST), map.get(User.LAST));

            return StringUtils.EMPTY;
        });

    }
}
