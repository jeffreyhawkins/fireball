package com.hawkins.fireball.controllers;

import com.hawkins.fireball.models.Company;
import com.hawkins.fireball.services.CompanyService;
import com.hawkins.fireball.utils.Constants;
import com.hawkins.fireball.utils.JsonTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static spark.Spark.*;

/**
 * Created by jhawkins on 7/21/16.
 */
public class CompanyController extends BaseController {
    private static final Logger LOG = LoggerFactory.getLogger(CompanyController.class);
    private CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;

        get("/rest/v1/companies", (req, res) -> {
            res.type(Constants.APPLICATION_JSON);
            return companyService.get();
        }, new JsonTransformer());


        get("/rest/v1/companies/:name", (req, res) -> {
            String name = req.params(":name");

            res.type(Constants.APPLICATION_JSON);
            return companyService.get(name);
        }, new JsonTransformer());


        post("/rest/v1/companies", (req, res) -> {
            String body = req.body();
            if (StringUtils.isBlank(body) == true) {
                halt(HttpServletResponse.SC_BAD_REQUEST, "Missing body.");
            }

            Map<String, String> map = getGson().fromJson(body, Map.class);

            companyService.add(map.get(Company.NAME));

            return StringUtils.EMPTY;
        });


        put("/rest/v1/companies/:id", (req, res) -> {
            String id = req.params(":id");

            String body = req.body();
            if (StringUtils.isBlank(body) == true) {
                halt(HttpServletResponse.SC_BAD_REQUEST, "Missing body.");
            }

            Map<String, String> map = getGson().fromJson(body, Map.class);

            companyService.update(id, map.get(Company.NAME));

            return StringUtils.EMPTY;
        });
    }
}
