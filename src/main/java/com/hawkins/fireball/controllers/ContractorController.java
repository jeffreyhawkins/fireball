package com.hawkins.fireball.controllers;

import com.hawkins.fireball.models.Contractor;
import com.hawkins.fireball.services.ContractorService;
import com.hawkins.fireball.utils.Constants;
import com.hawkins.fireball.utils.JsonTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.post;

/**
 * Created by jhawkins on 7/21/16.
 */
public class ContractorController extends BaseController {
    private static final Logger LOG = LoggerFactory.getLogger(ContractorController.class);
    private ContractorService contractorService;

    public ContractorController(ContractorService contractorService) {
        LOG.info("ContractorController::ctor");

        this.contractorService = contractorService;

        get("/rest/v1/contractors", (req, res) -> {
            String company = req.headers(Constants.ORGOID);

            return contractorService.query();
        }, new JsonTransformer());


        post("/rest/v1/contractors", (req, res) -> {
            String body = req.body();
            if (StringUtils.isBlank(body) == true) {
                halt(HttpServletResponse.SC_BAD_REQUEST, "Missing body.");
            }

            Map<String, String> map = getGson().fromJson(body, Map.class);

            contractorService.put(
                    map.get(Contractor.COMPANY_ID),
                    map.get(Contractor.CATEGORY),
                    map.get(Contractor.CONTACT),
                    map.get(Contractor.NAME),
                    map.get(Contractor.ADDRESS),
                    map.get(Contractor.CITY),
                    map.get(Contractor.STATE),
                    map.get(Contractor.ZIP),
                    map.get(Contractor.PHONE),
                    map.get(Contractor.WEBSITE));

            return StringUtils.EMPTY;
        });

    }
}
