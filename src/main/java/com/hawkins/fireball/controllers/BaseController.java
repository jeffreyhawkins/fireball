package com.hawkins.fireball.controllers;

import com.google.gson.Gson;

/**
 * Created by jhawkins on 7/23/16.
 */
public class BaseController {
    private Gson gson;

    protected BaseController() {
        gson = new Gson();
    }

    protected Gson getGson() {
        return gson;
    }

}
