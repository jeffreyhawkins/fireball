package com.hawkins.fireball.services;

import com.hawkins.fireball.datasources.SupplierDataSource;
import com.hawkins.fireball.models.Supplier;
import com.hawkins.fireball.builders.SupplierBuilder;
import com.hawkins.fireball.validations.SupplierValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by jhawkins on 7/21/16.
 */
public class SupplierService {
    private static final Logger LOG = LoggerFactory.getLogger(SupplierService.class);
    private SupplierDataSource supplierDataSource;

    public SupplierService() {
        LOG.info("SupplierService::ctor");
        supplierDataSource = new SupplierDataSource();
    }


    public List<Supplier> query() {
        LOG.info("SupplierService::query");

        return supplierDataSource.query();
    }


    public void put(String companyId, String contact, String name, String address, String city, String state,
                    String zip, String phone, String website) {
        LOG.info("SupplierService::put");

        Supplier supplier = new SupplierBuilder()
                .setCompanyId(companyId.trim())
                .setContact(contact.trim())
                .setName(name.trim())
                .setAddress(address.trim())
                .setCity(city.trim())
                .setState(state.trim())
                .setZip(zip.trim())
                .setPhone(phone.trim())
                .setWebsite(website.trim())
                .build();

        if (SupplierValidator.isValid(supplier) == false) {
            throw new IllegalStateException("Invalid supplier.");
        }

        supplierDataSource.put(supplier);
    }


    @Override
    public String toString() {
        return "SupplierService";
    }
}
