package com.hawkins.fireball.services;

import com.hawkins.fireball.datasources.CompanyDataSource;
import com.hawkins.fireball.models.Company;
import com.hawkins.fireball.builders.CompanyBuilder;
import com.hawkins.fireball.validations.CompanyValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by jhawkins on 7/21/16.
 */
public class CompanyService {
    private static final Logger LOG = LoggerFactory.getLogger(CompanyService.class);
    private CompanyDataSource companyDataSource;

    public CompanyService() {
        companyDataSource = new CompanyDataSource();
    }


    public List<Company> get() {
        LOG.info("CompanyService::get");

        return companyDataSource.queryMongo();
    }


    public List<Company> get(String name) {
        LOG.info("CompanyService::get");

        Company company = new CompanyBuilder()
                .setName(name.trim())
                .build();

        return companyDataSource.query(company);
    }


    public void add(String name) {
        LOG.info("CompanyService::put");

        Company company = new CompanyBuilder()
                .setName(name.trim())
                .build();

        if (!CompanyValidator.isValid(company)) {
            throw new IllegalStateException("Invalid company.");
        }

        companyDataSource.putMongo(company);
    }


    public void update(String id, String name) {
        LOG.info("CompanyService::update");

        Company company = new CompanyBuilder()
                .setId(id.trim())
                .setName(name.trim())
                .build();

        if (!CompanyValidator.isValid(company)) {
            throw new IllegalStateException("Invalid company.");
        }

        companyDataSource.put(company);
    }


    @Override
    public String toString() {
        return "CompanyService";
    }
}
