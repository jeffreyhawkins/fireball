package com.hawkins.fireball.services;

import com.hawkins.fireball.datasources.ContractorDataSource;
import com.hawkins.fireball.models.Contractor;
import com.hawkins.fireball.builders.ContractorBuilder;
import com.hawkins.fireball.validations.ContractorValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by jhawkins on 7/21/16.
 */
public class ContractorService {
    private static final Logger LOG = LoggerFactory.getLogger(ContractorService.class);
    private ContractorDataSource contractorDataSource;


    public ContractorService() {
        LOG.info("ContractorService::ctor");
        contractorDataSource = new ContractorDataSource();
    }


    public List<Contractor> query() {
        LOG.info("ContractorService::query");

        return contractorDataSource.query();
    }


    public void put(String companyId, String category, String contact, String name, String address, String city, String state,
                    String zip, String phone, String website) {
        LOG.info("ContractorService::put");

        Contractor contractor = new ContractorBuilder()
                .setCompanyId(companyId.trim())
                .setCategory(category.trim())
                .setContact(contact.trim())
                .setName(name.trim())
                .setAddress(address.trim())
                .setCity(city.trim())
                .setState(state.trim())
                .setZip(zip.trim())
                .setPhone(phone.trim())
                .setWebsite(website.trim())
                .build();

        if (ContractorValidator.isValid(contractor) == false) {
            throw new IllegalStateException("Invalid contractor.");
        }

        contractorDataSource.put(contractor);
    }


    @Override
    public String toString() {
        return "ContractorService";
    }
}
