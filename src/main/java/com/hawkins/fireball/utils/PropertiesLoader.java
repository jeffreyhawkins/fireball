package com.hawkins.fireball.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by jhawkins on 7/21/16.
 */
public class PropertiesLoader {
    private static final Logger LOG = LoggerFactory.getLogger(PropertiesLoader.class);

    public static void load(String name) {
        try (InputStream in = PropertiesLoader.class.getResourceAsStream(name)) {
            Properties properties = new Properties();
            properties.load(in);
            System.setProperties(properties);
        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }
}
