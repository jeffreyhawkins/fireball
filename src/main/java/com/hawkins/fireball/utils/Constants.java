package com.hawkins.fireball.utils;

/**
 * Created by jhawkins on 7/25/16.
 */
public final class Constants {
    public static final String ORGOID = "orgoid";
    public static final String APPLICATION_JSON = "application/json";

    private Constants() {}
}
