package com.hawkins.fireball.models;

/**
 * Created by jhawkins on 7/21/16.
 */
public class Company {
    public static final String ID = "id";
    public static final String NAME = "name";

    private String id;
    private String name;

    public Company(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Id=" + id +
                " Name=" + name;
    }
}
