package com.hawkins.fireball.models;

/**
 * Created by jhawkins on 7/21/16.
 */
public class Supplier {
    public static final String ID = "id";
    public static final String COMPANY_ID = "companyId";
    public static final String CONTACT = "contact";
    public static final String NAME = "name";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String PHONE = "phone";
    public static final String WEBSITE = "website";

    private String id;
    private String companyId;
    private String contact;
    private String name;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String phone;
    private String website;


    public Supplier(String id, String companyId, String contact, String name,
                    String address, String city, String state, String zip,
                    String phone, String website) {
        this.id = id;
        this.companyId = companyId;
        this.contact = contact;
        this.name = name;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.phone = phone;
        this.website = website;
    }

    public String getId() {
        return id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public String getContact() {
        return contact;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    @Override
    public String toString() {
        return "Id=" + id +
                " CompanyId=" + companyId +
                " Contact=" + contact +
                " Name=" + name +
                " Address=" + address +
                " City=" + city +
                " State=" + state +
                " Zip=" + zip +
                " Phone=" + phone +
                " Website=" + website;
    }
}
