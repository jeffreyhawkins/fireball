package com.hawkins.fireball.builders;

import com.hawkins.fireball.models.Supplier;

/**
 * Created by jhawkins on 7/25/16.
 */
public class SupplierBuilder {
    private String id;
    private String companyId;
    private String contact;
    private String name;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String phone;
    private String website;

    public SupplierBuilder() {}

    public SupplierBuilder setId(String id) {
        this.id = id;

        return this;
    }

    public SupplierBuilder setCompanyId(String companyId) {
        this.companyId = companyId;

        return this;
    }

    public SupplierBuilder setContact(String contact) {
        this.contact = contact;

        return this;
    }

    public SupplierBuilder setName(String name) {
        this.name = name;

        return this;
    }

    public SupplierBuilder setAddress(String address) {
        this.address = address;

        return this;
    }

    public SupplierBuilder setCity(String city) {
        this.city = city;

        return this;
    }

    public SupplierBuilder setState(String state) {
        this.state = state;

        return this;
    }

    public SupplierBuilder setZip(String zip) {
        this.zip = zip;

        return this;
    }

    public SupplierBuilder setPhone(String phone) {
        this.phone = phone;

        return this;
    }

    public SupplierBuilder setWebsite(String website) {
        this.website = website;

        return this;
    }

    public Supplier build() {
        return new Supplier(id, companyId, contact, name, address, city, state, zip, phone, website);
    }
}
