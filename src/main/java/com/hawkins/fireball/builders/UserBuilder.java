package com.hawkins.fireball.builders;

import com.hawkins.fireball.models.User;

/**
 * Created by jhawkins on 7/23/16.
 */
public class UserBuilder {
    private User user;

    public UserBuilder() {
        user = new User();
    }


    public UserBuilder setId(String id) {
        user.setId(id);

        return this;
    }


    public UserBuilder setCompanyId(String companyId) {
        user.setCompanyId(companyId);

        return this;
    }


    public UserBuilder setName(String name) {
        user.setName(name);

        return this;
    }


    public UserBuilder setEmail(String email) {
        user.setEmail(email);

        return this;
    }

    public UserBuilder setPassword(String password) {
        user.setPassword(password);

        return this;
    }

    public UserBuilder setResetCode(String resetCode) {
        user.setResetCode(resetCode);

        return this;
    }

    public UserBuilder setAdmin(boolean admin) {
        user.setAdmin(admin);

        return this;
    }

    public UserBuilder setLocked(boolean locked) {
        user.setLocked(locked);

        return this;
    }


    public User build() {
        return user;
    }
}
