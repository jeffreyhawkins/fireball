package com.hawkins.fireball.builders;

import com.hawkins.fireball.models.Contractor;

/**
 * Created by jhawkins on 7/25/16.
 */
public class ContractorBuilder {
    private String id;
    private String companyId;
    private String category;
    private String contact;
    private String name;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String phone;
    private String website;

    public ContractorBuilder() {}

    public ContractorBuilder setId(String id) {
        this.id = id;

        return this;
    }

    public ContractorBuilder setCompanyId(String companyId) {
        this.companyId = companyId;

        return this;
    }

    public ContractorBuilder setCategory(String category) {
        this.category = category;

        return this;
    }

    public ContractorBuilder setContact(String contact) {
        this.contact = contact;

        return this;
    }

    public ContractorBuilder setName(String name) {
        this.name = name;

        return this;
    }

    public ContractorBuilder setAddress(String address) {
        this.address = address;

        return this;
    }

    public ContractorBuilder setCity(String city) {
        this.city = city;

        return this;
    }

    public ContractorBuilder setState(String state) {
        this.state = state;

        return this;
    }

    public ContractorBuilder setZip(String zip) {
        this.zip = zip;

        return this;
    }

    public ContractorBuilder setPhone(String phone) {
        this.phone = phone;

        return this;
    }

    public ContractorBuilder setWebsite(String website) {
        this.website = website;

        return this;
    }

    public Contractor build() {
        return new Contractor(id, companyId, category, contact, name, address, city, state, zip, phone, website);
    }
}
