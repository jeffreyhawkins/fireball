package com.hawkins.fireball.builders;

import com.hawkins.fireball.models.Company;

/**
 * Created by jhawkins on 7/23/16.
 */
public class CompanyBuilder {
    private String id;
    private String name;

    public CompanyBuilder(){}


    public CompanyBuilder setId(String id) {
        this.id = id;

        return this;
    }


    public CompanyBuilder setName(String name) {
        this.name = name;

        return this;
    }

    public Company build() {
        return new Company(id, name);
    }
}
