package com.hawkins.fireball.validations;

import com.hawkins.fireball.models.Supplier;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jhawkins on 8/23/16.
 */
public class SupplierValidator {
    private static final Logger LOG = LoggerFactory.getLogger(SupplierValidator.class);
    private static final int FIELD_LENGTH = 100;


    public static boolean isValid(Supplier supplier) {
        LOG.info("SupplierValidator::isValid");

        if (supplier == null) {
            LOG.info("supplier is null");
            return false;
        }

        if (StringUtils.isEmpty(supplier.getCompanyId())) {
            LOG.info("supplier companyId is empty");
            return false;
        }

        if (StringUtils.isEmpty(supplier.getName())) {
            LOG.info("supplier name is empty");
            return false;
        }

        if (supplier.getName().length() > FIELD_LENGTH) {
            LOG.info("supplier name is too long");
            return false;
        }

        if (StringUtils.isEmpty(supplier.getContact())) {
            LOG.info("supplier contact is empty");
            return false;
        }

        if (supplier.getContact().length() > FIELD_LENGTH) {
            LOG.info("supplier contact is too long");
            return false;
        }

        if (StringUtils.isEmpty(supplier.getPhone())) {
            LOG.info("supplier phone is empty");
            return false;
        }

        return true;
    }
}
