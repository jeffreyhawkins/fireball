package com.hawkins.fireball.validations;

import com.hawkins.fireball.models.Company;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jhawkins on 8/23/16.
 */
public class CompanyValidator {
    private static final Logger LOG = LoggerFactory.getLogger(CompanyValidator.class);
    private static final int FIELD_LENGTH = 100;


    public static boolean isValid(Company company) {
        LOG.info("CompanyValidator::isValid");

        if (company == null) {
            LOG.info("company is null");
            return false;
        }

        if (StringUtils.isEmpty(company.getName())) {
            LOG.info("company name is empty");
            return false;
        }

        if (company.getName().length() > FIELD_LENGTH) {
            LOG.info("company name is too long");
            return false;
        }

        return true;
    }
}
