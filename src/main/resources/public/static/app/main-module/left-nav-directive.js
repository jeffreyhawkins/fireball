angular.module('build').directive('leftNav', function($timeout) {
  return {
      restrict: 'C',
      link: function ($scope, element, attrs) {
        element.find('li').each(function(index, el) {
          angular.element(el).on('click', function(event) {

            element.find('li').each(function(i, e) {
              angular.element(e).removeClass('selected');
            });

            angular.element(el).addClass('selected');
          });
        });
      }
  };
});