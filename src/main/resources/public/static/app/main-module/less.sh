#!/bin/sh

lessc alerts.less > alerts.css
lessc top-nav.less > top-nav.css
lessc view.less > view.css
lessc left-nav.less > left-nav.css
lessc home.less > home.css
