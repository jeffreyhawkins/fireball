'use strict';

angular.module('build').service('AlertService',
	function($timeout) {

		var impl = {

			close: function() {
				$timeout(function(self) {
				impl.hideAll();
			}, 3000);
		},

		showSuccessMessage: function (message) {
			$('.alerts').find('.alert-success').find('.alert-message').html('<i class="fa fa-check-circle"></i>&nbsp' + message);
			$('.alerts').find('.alert-success').removeClass('hidden');
			impl.close();
		},

		showInfoMessage: function(message) {
			$('.alerts').find('.alert-info').find('.alert-message').html('<i class="fa fa-info-circle"></i>&nbsp' + message);
			$('.alerts').find('.alert-info').removeClass('hidden');
			impl.close();
		},

		showWarningMessage: function(message) {
			$('.alerts').find('.alert-warning').find('.alert-message').html('<i class="fa fa-exclamation-triangle"></i>&nbsp' + message);
			$('.alerts').find('.alert-warning').removeClass('hidden');
			impl.close();
		},

		showDangerMessage: function(message) {
			$('.alerts').find('.alert-danger').find('.alert-message').html('<i class="fa fa-exclamation-circle"></i>&nbsp' + message);
			$('.alerts').find('.alert-danger').removeClass('hidden');
			impl.close();
		},

        hideAll: function() {
            $('.alerts').find('.alert-success').addClass('hidden');
			$('.alerts').find('.alert-info').addClass('hidden');
			$('.alerts').find('.alert-warning').addClass('hidden');
			$('.alerts').find('.alert-danger').addClass('hidden');
        }
	};

	return impl;
});