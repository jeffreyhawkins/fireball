
'use strict';

angular.module('build').service('AuthenticationService',
  function() {
    var impl = {
      _signedIn: false,
      
      signIn: function () {
        impl._signedIn = true;
      },
      
      signOut: function () {
        impl._signedIn = false;
      },
      
      isSignedIn: function () {
        return impl._signedIn;
      }
    };
    
    return impl;
});