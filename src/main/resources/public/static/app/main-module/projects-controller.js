'use strict';

angular.module('build').controller('ProjectsController', [
  '$scope',
  '$log',
  '$http',
  function ($scope, $log, $http) {
    $scope.projects = [];


    $scope.init = function() {

      var config = {
        'headers' : {
          'CID' : 'ahBkZXZ-amVmZi1oYXdraW5zchQLEgdDb21wYW55GICAgICAgIAKDA'
        }
      };

      $http.get('/rest/v1/projects', config).then(function(result) {
        $scope.projects = result.data;
      });
    }

    $scope.init();
  }
]);
