'use strict';

angular.module('build', [
	'ngRoute',
	'ngAnimate',
	'ui.router',
	'xfit.calendar'
]);
