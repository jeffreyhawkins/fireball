'use strict';

angular.module('build').service('SignInService',
  function($q, $http) {
    var impl = {
    
      signIn: function (login, password) {
        var deferred = $q.defer();
        
        var data = {
          user: login,
          password: password
        };
        
        $http.post('/xfit/v1/login', data).then(function(response) {
          deferred.resolve(response);
          
          // TODO:
          // 1. Set cookie with returned token
          // 2. Call Authentication.signIn();
          // 3. Set cookie with role
          // 4. Set cookie with name
          // 5. Call RoleService.set(role);
          
        }, function(response) {
          deferred.reject(response);
        });
        
        return deferred.promise;
      },
      
      signOut: function() {
        // TODO:
        // 1. Remove token cookie
        // 2. Call AuthenticationService.signOut();
        // 3. Remove role cookie
        // 4. Remove name cookie
        // 5. Redirect to sign out state
      }
    };
    
    return impl;
});