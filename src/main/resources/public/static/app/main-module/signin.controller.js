'use strict';

angular.module('build').controller('SignInController', [
  '$scope',
  '$log',
  function ($scope, $log) {

    $scope.credentials = {
      email: '',
      password: '',
      error: ''
    };

    $scope.onSignInClick = function() {
      $log.log('You clicked on sign in.');
    };
    
    $scope.onForgotPasswordClick = function() {
      $log.log('You clicked on forgot password.');
    };
    
    $scope.onSignUpClick = function() {
      $log.log('You clicked on sign up.');
    };
    
  }
]);
