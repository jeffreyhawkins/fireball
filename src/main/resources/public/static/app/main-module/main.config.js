'use strict';

angular.module('build').config([
	'$stateProvider',
	function($stateProvider) {
		$stateProvider.state('index', {
				url: '/',
				controller: 'IndexController',
				templateUrl: 'app/main-module/home.html',
				controllerAs: 'controller'
			}),
		$stateProvider.state('projects', {
				url: '/projects',
				controller: 'ProjectsController',
				templateUrl: 'app/main-module/projects.html',
				controllerAs: 'controller'
			}),
		$stateProvider.state('contractors', {
				url: '/contractors',
				controller: 'ContractorsController',
				templateUrl: 'app/main-module/contractors.html',
				controllerAs: 'controller'
			}),
		$stateProvider.state('suppliers', {
				url: '/suppliers',
				controller: 'SuppliersController',
				templateUrl: 'app/main-module/suppliers.html',
				controllerAs: 'controller'
			}),
		$stateProvider.state('equipment', {
				url: '/equipment',
				controller: 'EquipmentController',
				templateUrl: 'app/main-module/equipment.html',
				controllerAs: 'controller'
			}),
		$stateProvider.state('budgets', {
				url: '/budgets',
				controller: 'BudgetsController',
				templateUrl: 'app/main-module/budgets.html',
				controllerAs: 'controller'
			}),
		$stateProvider.state('files', {
				url: '/files',
				controller: 'FilesController',
				templateUrl: 'app/main-module/files.html',
				controllerAs: 'controller'
			}),
		$stateProvider.state('profile', {
				url: '/profile',
				controller: 'ProfileController',
				templateUrl: 'app/main-module/profile.html',
				controllerAs: 'controller'
			})
	}
]);
