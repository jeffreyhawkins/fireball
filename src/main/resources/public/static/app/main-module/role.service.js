
'use strict';

angular.module('build').service('RoleService',
  function() {
    var impl = {
      _role: -1,
      
      set: function (role) {
        impl._role = role;
      },
      
      get: function () {
        return impl._role;
      },
      
      description: function () {
        if (impl._role === 1) {
          return 'Athlete';
        } else if (impl._role === 2) {
          return 'Trainer';
        } else if (impl._role === 3) {
          return 'Administrator';
        }
        
        throw 'Invalid role!';
      }
    };
    
    return impl;
});