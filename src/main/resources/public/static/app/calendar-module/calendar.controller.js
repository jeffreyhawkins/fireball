'use strict';

angular.module('xfit.calendar').controller('CalendarController', [
    '$rootScope',
    '$scope',
    '$log',
    function ($rootScope, $scope, $log) {
      
      $log.log('Loaded calendar controller...');
      
      $scope.months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];

      $scope.days = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ];

      $scope.currentClick = {};

      $scope.currentDate = new Date();
      $scope.month = $scope.months[$scope.currentDate.getMonth()];
      $scope.year = $scope.currentDate.getFullYear();

      $scope.previousMonth = function() {
        var year = $scope.currentDate.getFullYear();
        var month = $scope.currentDate.getMonth();
        var day = $scope.currentDate.getDate();

        if (month === 0) {
          month = 11;
          year -= 1;
        } else {
          month -= 1;
        }

        $scope.currentDate = new Date(year, month, day);
        $scope.month = $scope.months[$scope.currentDate.getMonth()];
        $scope.year = $scope.currentDate.getFullYear();
        $scope.layout();
      };

      $scope.nextMonth = function() {
        var year = $scope.currentDate.getFullYear();
        var month = $scope.currentDate.getMonth();
        var day = $scope.currentDate.getDate();
        if (month === 11) {
          month = 0;
          year += 1;
        } else {
          month += 1;
        }

        $scope.currentDate = new Date(year, month, day);
        $scope.month = $scope.months[$scope.currentDate.getMonth()];
        $scope.year = $scope.currentDate.getFullYear();
        $scope.layout();
      };

      $scope.layout = function() {
        $scope.weeks = [];

        var start = angular.copy($scope.currentDate, start);
        start.setDate(1);

        var nextMonth = (start.getMonth() + 1) % 12;
        $scope.weeks = [ { days: [] } ];

        var week = $scope.weeks[0];

        do {
          if(start.getDay() === 0 && start.getDate() !== 1) {
            week = { days: [] };
            $scope.weeks.push(week);
          }

          week.days[start.getDay()] = start.getDate();

          start.setDate(start.getDate() + 1);
        } while(start.getMonth() !== nextMonth);

        while(week.days.length < 7) {
          week.days.push(undefined);
        }
      };

      $scope.onWeekClick = function(index) {
        $scope.currentClick.week = index;
        $scope.currentClick.month = $scope.currentDate.getMonth();
        $scope.currentClick.year = $scope.currentDate.getFullYear();
        $scope.currentClick.day = $scope.weeks[index].days[$scope.currentClick.day];

        var day = $scope.currentClick.day;
        if (day === undefined) {
          $log.log('clicked invalid day!');
        }
      };

      $scope.onDayClick = function(index) {
        $scope.currentClick.day = index;
      };

      $scope.isToday = function(day) {
        if (!day) {
          return;
        }

        var today = new Date();

        if ($scope.currentDate.getDate() === day) {
          if ($scope.currentDate.getMonth() === today.getMonth()) {
            if ($scope.currentDate.getFullYear() === today.getFullYear()) {
              return 'today'; // class name for highlighting current day.
            }
          }
        }

        return;
      };

      $scope.layout();
    }
  ]);
