'use strict';

angular.module('xfit.calendar').config([
	'$stateProvider',
	function($stateProvider) {
		$stateProvider.state('calendar', {
				url: '/clendar',
				controller: 'CalendarController',
				templateUrl: 'app/calendar-module/calendar.template.html',
				controllerAs: 'controller'
			}
		)}
]);
