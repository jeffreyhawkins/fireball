create table companies (
id      char(36) not null primary key,
name    varchar(100) not null
) engine=innodb,charset=utf8;

insert into companies values (uuid(), 'Demo Inc.');
insert into companies values (uuid(), 'Munsters Co.');
insert into companies values (uuid(), 'Gilligan\'s Garage');


create table users (
id         char(36) not null primary key,
companyId  char(36) not null,
email      varchar(100) not null,
password   varchar(100) not null,
locked     char(1) not null,
admin      char(1) not null,
code       char(36),
verified   char(1) not null
) engine=innodb,charset=utf8;

-- Hawk001$
-- $2a$10$WrtU9o2kbnTwGvlqckpMrexareXU7FRpmRGpeI/O6ugkFy0RKk8SG

insert into users values (uuid(), (select id from companies where name = 'Demo Inc.'), 'jeff@demo.com', '$2a$10$WrtU9o2kbnTwGvlqckpMrexareXU7FRpmRGpeI/O6ugkFy0RKk8SG', 'N', 'Y', NULL, 'Y');


create table suppliers (
id         char(36) not null primary key,
companyId  char(36) not null,
contact    varchar(100),
name       varchar(100) not null,
address    varchar(100),
city       varchar(100),
state      varchar(50),
zip        varchar(20),
phone      varchar(20),
website    varchar(100)
) engine=innodb,charset=utf8;

insert into suppliers values (uuid(), (select id from companies where name = 'Demo Inc.'),
    'Susie Supplier', 'Demo Supplier', '123 Main Street', 'Canton', 'GA', '30115', '555-1212', null);


create table contractors (
id         char(36) not null primary key,
companyId  char(36) not null,
category   varchar(255),
contact    varchar(100),
name       varchar(100) not null,
address    varchar(100),
city       varchar(100),
state      varchar(50),
zip        varchar(20),
phone      varchar(20),
website    varchar(100)
) engine=innodb,charset=utf8;

insert into contractors values (uuid(), (select id from companies where name = 'Demo Inc.'),
    'Drywall', 'Dusty Jones', 'Drywall Are Us', '124 Main Street', 'Canton', 'GA', '30115', '555-1212', null);
